#!/bin/bash
# Copyright (C) Intel Corp.  2024.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Yuriy Bogdanov <yuriy.bogdanov@intel.com>
#  **********************************************************************/


LDCONF="/etc/ld.so.conf.d/01-mesa.conf"

check_sudo(){
   local REALID=$(id -u)
   if [ "$REALID" != "0" ]; then
      # exec sudo "$BASH" "$0" "$@"
      eval $(cat /etc/lsb-release)
      # echo "The installer needs to be executed with sudo."
      # exit -1
   fi
}

check_vars(){
   if [ ! -v BASE ]; then BASE='/opt/mesa'; fi
   if [ ! -v BUILD ]; then BUILD='build'; fi
   if [ ! -v INSTALL ]; then INSTALL='install'; fi
   if [ ! -v SRC ]; then SRC='src'; fi

}
# set_link() {
#    if [ -e "$BASE/$INSTALL" ]; then
#       local CURLINK=$(realpath $BASE/$INSTALL)
#       if [ $CURLINK != $INSTALLPATH ]; then
#          # FIXME: This seems to be broken. If the link doesn't exist, $CURLINK
#          # will be /opt/mesa, which could be removed.
#          echo "Removing old link to: $CURLINK"
#          rm -rf $BASE/$INSTALL
#       fi
#    fi

#    if [ ! -e "$BASE/$INSTALL" ]; then
#       echo "Setting up link $BASE/$INSTALL -> $$(tar xf $latestar install/build_info -O)..."
#       ln -s $INSTALLPATH $BASE/$INSTALL
#    fi
# }

set_ldconfig() {
   echo "Installing ldconfig settings to $LDCONF..."
   rm -rf $LDCONF

   cat > $LDCONF << EOF
$BASE/$INSTALL/64/lib
$BASE/$INSTALL/32/lib
$BASE/$INSTALL/64/lib/x86_64-linux-gnu
$BASE/$INSTALL/32/lib/i386-linux-gnu
EOF

   ldconfig
}

set_vk_icd(){
   echo "Setting up default VK_ICD_FILENAMES..."

   local VK_ICD_FILENAMES="$BASE/$INSTALL/64/share/vulkan/icd.d/intel_icd.x86_64.json:$BASE/$INSTALL/32/share/vulkan/icd.d/intel_icd.i686.json"
   local ICDLINE=$(grep '^VK_ICD_FILENAMES=' /etc/environment)

   if [ -n "$ICDLINE" ]; then
      if [ "${ICDLINE/VK_ICD_FILENAMES=}" != "$VK_ICD_FILENAMES" ]; then
         cp /etc/environment /etc/environment.bak
         sed -i -e "s,^VK_ICD_FILENAMES=.*,VK_ICD_FILENAMES=${VK_ICD_FILENAMES}," /etc/environment
      fi
   else
      echo "VK_ICD_FILENAMES=${VK_ICD_FILENAMES}" >> /etc/environment
   fi
}

build(){
   cmd="docker-compose run tar python3 -u build_mesa.py"

   # Append modifications of the path to main command
   if [ -v BASE ]; then cmd=${cmd}" --mesa_base "${BASE}; fi
   if [ -v BUILD ]; then cmd=${cmd}" --mesa_build "${BUILD}; fi
   if [ -v INSTALL ]; then cmd=${cmd}" --mesa_install "${INSTALL}; fi
   if [ -v REPO ]; then cmd=${cmd}" --repo "${REPO}; fi
   if [ -v SRC ]; then cmd=${cmd}" --mesa_src "${SRC}; fi
   if [ -v SHA ]; then cmd=${cmd}" --sha "${SHA}; fi

   echo $cmd
   eval $cmd
}

install(){
   # build
   # ToDo: check is distro is the same as tar
   latestar=$( ls -t mesa_build/*.tar.xz | head -n1 )

   # Get latest build info 
   build_information=$(tar xf $latestar install/build_info -O)
   eval $build_information
   TAR_DISTRIB_ID=$DISTRIB_ID
   TAR_DISTRIB_RELEASE=$DISTRIB_RELEASE

   # Compare build with running OS
   if [ -e /etc/lsb-release ]; then
      eval $(cat /etc/lsb-release)
      # declare -l $DISTRIB_ID
         if [ "${DISTRIB_ID,,}" != "${TAR_DISTRIB_ID,,}" ]; then
            if [ ${DISTRIB_RELEASE,,} != ${TAR_DISTRIB_RELEASE,,} ]; then
               echo "ERROR: Installer requires $TAR_DISTRIB_ID:$TAR_DISTRIB_RELEASE"
               exit -1 
            fi
         fi
   else
      DISTRIB_ID=$(lsb_release -si)
      DISTRIB_RELEASE=$(lsb_release -sr)
      if [ "${DISTRIB_ID,,}" != ${TAR_DISTRIB_ID,,} ]; then
         if [ "${DISTRIB_RELEASE,,}" != "${TAR_DISTRIB_RELEASE,,}" ]; then
            echo "ERROR: Installer requires $TAR_DISTRIB_ID:$TAR_DISTRIB_RELEASE"
            exit -1
         fi
      fi
   fi

   # untar latest mesa build
   echo "Extracting content of the tar: '$latestar'"

   if [ -z ${INSTALL} ]; then
      if [ -d "$BASE/$INSTALL" ]; then
         echo "removing old $BASE/*"
         rm -rf $BASE/*
      else
         echo "Creating $BASE"
         mkdir $BASE 
      fi
      echo "RUN: tar xf $latestar -C $BASE"
      tar xf $latestar -C $BASE 
   else
      if [ -d "$BASE/$INSTALL" ]; then
         echo "removing old $BASE/*"
         rm -rf $BASE/*

      else
         # ToDO: expand or remve base dir modifier permenantly
         echo "Creating $BASE"
         mkdir $BASE
      fi
      echo "RUN: tar xf $latestar -C $BASE"
      tar xf $latestar -C "$BASE" 
   fi
   # set_link
   set_ldconfig
   set_vk_icd
}

clean(){
   rm -rf mesa_build/*.tar.xz

}

uninstall(){
   if  [ -v clean == true ]; then
      clean
   fi

   rm -rf $LDCONF
   # rm -rf $BASEINSTALLPATH
   cp /etc/environment /etc/environment.bak
   sed -i -e '/^VK_ICD_FILENAMES=/d' /etc/environment
   ldconfig
}

help(){
   clear
   echo "Avalable Arguments to pass"
   echo "-h  --help              To see this help again!"
   echo
   echo "-b  -build              Will build tar but will not install in default location."
   echo "-i  -install            Will build tar and install in default location."
   echo "-u  -uninstall          Will remove associated MESA files from defult location."
   echo
   echo "    --base=DIR          Path to default MESA location (default: /opt/mesa)"
   echo "    --src=DIR           Path to source files/repository (default: /opt/mesa/src) "
   echo "    --build_path=DIR    Path to MESA build directory. (default: /opt/mesa/build)"
   echo "    --install_path=DIR  Path to MESA binaries install directory (default: /opt/mesa/install)"
   echo "    --repo=URL          Git repo URL to custom repository. (default: main/latest)"
   echo
}

for i in "$@"; do
   case $i in
   -b|-build)
      build_mesa=true
      shift # past argument=value
      ;;
   -i|-install)
      install_mesa=true
      shift # past argument=value
      ;;
   -u|-uninstall)
      uninstall_mesa=true
      shift # past argument=value
      ;;
   --clean)
      clean=true
      shift # past argument=value
      ;;
   --base=*)
      BASE="${i#*=}"
      shift # past argument=value
      ;;
   --src=*)
      SRC="${i#*=}"
      shift # past argument=value
      ;;
   --build_path=*)
      BUILD="${i#*=}"
      shift # past argument=value
      ;;
   --install_path=*)
      INSTALL="${i#*=}"
      shift # past argument=value
      ;;
   --repo=*)
      REPO="${i#*=}"
      shift # past argument with no value
      ;;
   -h|--help)
      help
      shift # past argument with no value
      ;;
   -*|--*)
      echo "Unknown option $i"
      exit 1
      ;;
   *)
      ;;
  esac
done

# Check sudo first
check_sudo
check_vars

echo "BASE            = ${BASE}"
echo "SRC             = ${SRC}"
echo "BUILD           = ${BUILD}"
echo "INSTALL         = ${INSTALL}"
echo "REPO            = ${REPO}"
echo "build_mesa      = ${build_mesa}"
echo "install_mesa    = ${install_mesa}"
echo "uninstall_mesa  = ${uninstall_mesa}"

if [[ -n $1 ]]; then
    echo "Last line of file specified as non-opt/last argument:"
    tail -1 $1
fi

if [ "${build_mesa}" = true ]; then
   build
elif [ "${install_mesa}" = true ]; then
   install
elif [ "${uninstall_mesa}" = true ]; then
   uninstall
else
   echo "Could not proseed with selected options!"
fi

