#!/usr/bin/env python3
# Copyright (C) Intel Corp.  2024.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Yuriy Bogdanov <yuriy.bogdanov@intel.com>
#  **********************************************************************/

import argparse
import git
import requests
import os
import subprocess
import distro
# from timeout import timeout
import progress
import json

# NOTE: default build directories are provided by argparser 
# NOTE: default path: /opt/mesa
# NOTE: build repo:   /opt/mesa/repo
# NOTE: build path:   /opt/mesa/build
# NOTE: install path  /opt/mesa/install

proxy='http://proxy-chain.intel.com:911'
default_mesa_path = '/opt/mesa'

# Flag --repo will append to this list
default_mirrors = [
    'https://gitlab.freedesktop.org/mesa/mesa.git', # default mirror
    'git://otc-mesa-ci.jf.intel.com/git/mirror/mesa/origin'
]

# mason intel setup flaggs
# cmd: meson setup <build path> <--prefix MESA_INSTALL_PATH> <-flags>
intel_flags = "-Dbuildtype=release \
                -Dvulkan-drivers=intel \
                -Dplatforms=x11 \
                -Dgallium-drivers=swrast,iris \
                -Dllvm=enabled"

# helper Function
def str_strip(string):
    return " ".join(string.split())

# helper function
def build_info(sha, base_path, install_path):
    build_info = f'{ os.path.join(base_path, install_path) }/build_info'

    if not os.path.isdir(os.path.join(base_path, install_path)):
        print("Making Install directory")
        os.makedirs(os.path.join(base_path, install_path))
    if os.path.isfile(build_info):
        os.remove(build_info)

    with open(build_info, 'w') as f:
        f.write(f'DISTRIB_ID={ distro.id() }\n')
        f.write(f'DISTRIB_RELEASE={ distro.version() }\n')
        f.write(f'INSTALL_DIR={ install_path }\n')
        f.write(f'MESA_SHA={ sha }\n')
    pass

def check_sha(repo, sha):
    try:
        print(f"MESA SHA: {repo.rev_parse(sha)}")
    except git.exc.BadName: # sha provided is not in orign/main
        print("Could not find provided SHA")

def proxy_check(mirror):
     # Try setting proxy if intel network restricts access to gitlab
    try:
        response = requests.get(mirror, timeout=5).status_code
    except:
        response = -1

    if response != 200:
        print(f"Proxy detected: Setting proxy")
        os.environ['HTTP_PROXY'] = proxy
        os.environ['HTTPS_PROXY'] = proxy


# TODO change verbose mode from print to logging
def cmd(command: str, cwd=None, shell=False,):
    _cmd = []
    # expand cmd string into list
    for item in command.split(' '):
        _cmd.append(item)
    
    print(f'\n{" ".join(command.split())}') # print command that has been passed to wrapper
    process = subprocess.Popen(_cmd, cwd=cwd, shell=shell, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    for line in process.stdout:
        print(line, end='')
    process.wait()


def checkout (sha, base_path, src_path):
    repo_dir = os.path.join(base_path, src_path)
    latest_sha = sha
    # try clonining first
    try:
        print(f"Cloning: {default_mirrors[0]}")
        repo = git.Repo.clone_from(
            url=default_mirrors[0], 
            to_path=repo_dir,
            progress=progress.GitRemoteProgress(),
        )
        latest_sha = repo.commit(sha)
        print(f"Found commit 1: {latest_sha}\nPublished by: {latest_sha.author.name}\nDate:\t      {latest_sha.authored_datetime}\n")
    except git.GitCommandError as err:
        # if repo exist pull latest
        print(f"REALLY???: {git.Repo(repo_dir)}")
        repo = git.Repo(repo_dir)
        repo.remote('origin').pull('main', progress=progress.GitRemoteProgress())
        latest_sha = repo.commit(sha)
        print(f"Found commit 2: {latest_sha}\nPublished by: {latest_sha.author.name}\nDate:\t      {latest_sha.authored_datetime}\n")
    except Exception as err:
        print(f"Failed to get repository: {err}")
        pass
    
    # Set HEX value to latest_sha from default value
    # sha = repo.commit(sha)
    if latest_sha == 'origin/main':
        for url in default_mirrors:
            try:
                if url != default_mirrors[0]:
                    remote = repo.create_remote('alt', url)
                    remote.fetch(sha)
                repo.git.checkout(sha)
                sha = repo.commit(sha)
                print(f"Found commit 3: {sha}\nPublished by: {sha.author.name}\nDate:\t      {sha.authored_datetime}\n")
            except git.GitCommandError as err:
                # Handle the case when the commit is not found in the repository
                if 'bad object' in str(err):
                    print(f"Commit {sha} not found in repository at: {url}")
                elif 'remote already exists' in str(err):
                    print(f'Repository:   {url}')
                    # clean up incase 'remote' exist in remote list
                    repo.delete_remote('alt')
                    remote = repo.create_remote('alt', url)
                    remote.fetch()

                    repo.git.checkout(sha)
                    print(f"Found commit 4: {sha}\nPublished by: {sha.author.name}\nDate:\t      {sha.authored_datetime}\n")
                else:
                    # Handle other GitCommandError exceptions
                    print(f"Error in repository at {url}: {err}")
                    return -1
            except Exception as err:
                # Handle all other exceptions
                print(f"Error at repository at {url}: {err}")
                return -1
            finally:
                repo.delete_remote('alt')
    return repo.git.rev_parse(sha, short=True) # remove short tag if you want full sha or specify number
        

def setup_mesa(base_path, src_path, build_path, install_path ):
    # Setup meson
    _cmd = str_strip(f"meson setup \
                     { os.path.join(base_path, build_path) } \
                     --prefix { os.path.join(base_path, install_path) } \
                     { intel_flags } \
                    ")
    if os.path.exists(os.path.join(base_path, build_path)):
        _cmd ="".join([_cmd, ' --reconfigure'])

    cmd(_cmd, cwd=os.path.join(base_path, src_path))

    # Run build 
    _cmd = f"ninja -C { os.path.join(base_path, build_path) }"
    cmd(_cmd, cwd=os.path.join(base_path, src_path))

    cmd("ninja build", cwd=os.path.join(base_path, src_path))

    # Run install
    _cmd = f"ninja install -C { os.path.join(base_path, build_path) }"
    cmd(_cmd, cwd=os.path.join(base_path, src_path))


def build_tar(sha, base_path, install_path):
    # Run tar against mesa install package
    build_info(sha, base_path, install_path)

    tar_file = f'mesa_pkg_{sha}.tar.xz'

    if os.path.isfile(os.path.join(base_path, tar_file)):
        print(f"Removing old tar { tar_file }")
        os.remove(os.path.join(base_path, tar_file))

    _cmd = f"tar -C { base_path } -cvzf { tar_file } { install_path }"
    cmd(_cmd, base_path) # base_path
    print(f"TAR FILE: {tar_file}")

  

if __name__ == "__main__":
    argparser = argparse.ArgumentParser(description=
                                        "Build and tar all files MESA Driver",
                                        formatter_class=argparse.RawTextHelpFormatter)
    
    argparser.add_argument("--mesa_base", default=default_mesa_path, help="Default mesa directory. (Default: /opt/mesa)")
    argparser.add_argument("--mesa_src", default='src', help=f"Directory where MESA sourse points. (Default: /opt/mesa/src)")
    argparser.add_argument("--mesa_build", default='build', help="Directory where MESA bulds points. (defult: /opt/mesa/build)")
    argparser.add_argument("--mesa_install", default='install', help="Directory where MESA install points. (defult: /opt/mesa/install)")
    argparser.add_argument("--repo", help="Add custom/your git repository for mesa cache")
    argparser.add_argument("--sha", default='origin/main', help="Provide MESA SHA you want to build driver base on (Default: latest mesa sha)")
    args = argparser.parse_args()

    # print(f"BASE: {args.base}, SRC: {args.src}, BUILD: {args.build}, INSTALL: {args.install}, REPO: {args.repo}, SHA{args.sha} ")

    if args.repo:
        default_mirrors.append(args.repo)

    # create /opt/mesa if it doesnot exist or passed custom base path
    if args.mesa_base != default_mesa_path:
        print(f"Creating mesa repository directory")
        os.mkdir(args.mesa_base, mode=0o776)
        default_mesa_path = args.mesa_base

    # Check Proxy
    proxy_check(default_mirrors[0])

    # Checkout mesa repository and return sha
    sha = checkout(args.sha, args.mesa_base, args.mesa_src)

    if sha != -1:
        setup_mesa(args.mesa_base, args.mesa_src, args.mesa_build, args.mesa_install)
        build_tar(sha, args.mesa_base, args.mesa_install)
    else:
        print(f"Failed to check out mesa sha, returned '{sha}' SHA is not valid.")